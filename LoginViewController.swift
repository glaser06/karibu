//
//  LoginViewController.swift
//  Karibu
//
//  Created by Zaizen Kaegyoshi on 1/17/15.
//  Copyright (c) 2015 Zaizen Kaegyoshi. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBarHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login(sender: AnyObject) {
//        var mapView = storyboard?.instantiateViewControllerWithIdentifier("MapViewController") as MapViewController
//        mapView.username = textField.text
        addUser()
        (navigationController?.viewControllers[0] as MapViewController).username = textField.text
        navigationController?.popToRootViewControllerAnimated(true)
    }
    func addUser()
    {
        let url = NSURL(string: "http://54.164.125.123/api/user/add")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["user_name": textField.text]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
            data, response, error in
            if error != nil
            {
                println(error)
                return
            }
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: nil) as NSDictionary
            
        })
        uploadTask.resume()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
