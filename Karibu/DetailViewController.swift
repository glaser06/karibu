//
//  DetailViewController.swift
//  Karibu
//
//  Created by Zaizen Kaegyoshi on 1/18/15.
//  Copyright (c) 2015 Zaizen Kaegyoshi. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var eneoPtsLabel: UILabel!
    var annotation: MKDetailAnnotation!
    
    @IBOutlet weak var textView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidAppear(animated: Bool) {
        titleLabel.text = annotation.title
        textView.text = annotation.detail
        eneoPtsLabel.text = "\(annotation.ep)"
    }
    @IBAction func upVote(sender: AnyObject) {
        let url = NSURL(string: "http://54.164.125.123/api/location/upvote")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["id": annotation.id]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
            data, response, error in
            if error != nil
            {
                println(error)
                return
            }
            
            
            
        })
        uploadTask.resume()
        self.annotation.ep = self.annotation.ep + 1
        self.eneoPtsLabel.text = "\(self.annotation.ep)"
        
    }
    
    @IBAction func downVote(sender: AnyObject) {
        let url = NSURL(string: "http://54.164.125.123/api/location/downvote")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["id": annotation.id]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
            data, response, error in
            if error != nil
            {
                println(error)
                return
            }
            
            
        })
        uploadTask.resume()
        self.annotation.ep = self.annotation.ep - 1
        self.eneoPtsLabel.text = "\(self.annotation.ep)"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
