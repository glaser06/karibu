//
//  MKDetailAnnotation.swift
//  Karibu
//
//  Created by Zaizen Kaegyoshi on 1/18/15.
//  Copyright (c) 2015 Zaizen Kaegyoshi. All rights reserved.
//

import UIKit
import MapKit

class MKDetailAnnotation: NSObject, MKAnnotation {
   
    var title: String
    var coordinate: CLLocationCoordinate2D
    var subtitle: String
    var detail: String!
    var id: Int!
    var ep: Int!
    
    init(coordinate: CLLocationCoordinate2D, title:String, subtitle: String)
    {
        self.title = title
        self.coordinate = coordinate
        self.subtitle = subtitle
    }
    
    
    func setCoordinate(newCoordinate: CLLocationCoordinate2D) {
        self.coordinate = newCoordinate
    }
}
