//
//  ViewController.swift
//  Karibu
//
//  Created by Zaizen Kaegyoshi on 1/16/15.
//  Copyright (c) 2015 Zaizen Kaegyoshi. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var mapView: MKMapView!
    
//    @IBOutlet weak var selfLocation: UIButton!
    var locationManager: CLLocationManager!
    
    var menu: REMenu!
    var addMenu: REMenu!
    
    var communities:[String]!
    var interaction = true
    
    var currentCommunity: String = ""
    
    var scrollView: UIScrollView!
    
    var addMode: Bool = false
    
    var username: String = ""
    
    var annotations:[MKDetailAnnotation] = []
    
    var listShown: Bool = false
    
    var tableView: UITableView!
    
    @IBOutlet weak var bottomButton: UIButton!
    
    var tempLocation: CLLocationCoordinate2D!
    var tempName: String!
    var tempTextField: UITextField!
    var tempTextBox: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupLocationManager()
        let location = CLLocationCoordinate2D(
            latitude: locationManager.location.coordinate.latitude,
            longitude: locationManager.location.coordinate.longitude
        )
        let span = MKCoordinateSpanMake(0.05, 0.05)
        let region = MKCoordinateRegion(center: location, span: span)
        
        mapView.setRegion(region, animated: false)
        if username == ""
        {
            var loginView = storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as LoginViewController
            self.navigationController?.pushViewController(loginView, animated: true)
            return
        }
        
        
        
    }
    override func viewDidAppear(animated: Bool) {
        self.navigationController?.navigationBarHidden = false
        
        
        
        self.navigationItem.title = "Map"
        
        communities = []
        getCommunities()
        
        
        
        var list: [REMenuItem] = []
        
        scrollView = UIScrollView()
        scrollView.directionalLockEnabled = true
        scrollView.contentSize = CGSize(width: self.view.frame.width-80, height: 900)
        scrollView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.width-80, height: 400))
        scrollView.backgroundColor = UIColor.clearColor()
        
        scrollView.alpha = 0.8
        scrollView.bounces = false
        
        
        var menuitem2: REMenuItem = REMenuItem(customView: scrollView)
        list.append(menuitem2)
        
        
        self.menu = REMenu(items: list)
        self.menu.itemHeight = 500
        self.menu.bounce = false
        self.menu.backgroundColor = UIColor.whiteColor()
        self.menu.backgroundAlpha = 0.3
        self.menu.appearsBehindNavigationBar = false
        self.menu.liveBlur = true
        self.menu.liveBlurTintColor = UIColor.grayColor()
        self.menu.borderColor = UIColor.clearColor()
        
        var lgpr: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: "addPin:")
        lgpr.minimumPressDuration = 0.5
        mapView.addGestureRecognizer(lgpr)
        
        //        self.menu.borderWidth = 280
        
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Menu", style: UIBarButtonItemStyle.Plain, target: self, action: "toggleMenu")
        self.navigationController?.navigationBar.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBar.translucent = true
        self.navigationController?.navigationBar.barTintColor = UIColor.orangeColor()
        
        var menuButton: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        menuButton.setImage(UIImage(named: "logo100"), forState: .Normal)
        menuButton.frame = CGRect(x: 0, y: 0, width: -15, height: 45)
        menuButton.titleLabel?.text = "Menu"
        //        menuButton.backgroundColor = UIColor.blackColor()
        menuButton.addTarget(self, action: "loadMap", forControlEvents: .TouchUpInside)
        self.navigationItem.titleView = menuButton
        //        self.navigationItem.titleView?.bringSubviewToFront(menuButton)
        self.navigationController?.navigationBar.bringSubviewToFront(menuButton)
        
        if currentCommunity != ""
        {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "toggleAddMode")
        }
    }
    
    @IBAction func showListVIew(sender: UIButton!)
    {
        
        
        if !listShown
        {
            tableView = UITableView(frame: CGRect(x: 0, y: 638, width: self.view.frame.width, height: self.view.frame.height-90))
            tableView.delegate = self
            tableView.dataSource = self
            tableView.alpha = 0.9
            self.view.addSubview(tableView)
            tableView.backgroundColor = UIColor.whiteColor()
            self.view.bringSubviewToFront(tableView)
            UIView.animateWithDuration(0.3, delay: 0, options: .CurveEaseIn, animations: {
                
                var rect = CGRect(x: 0, y: 60, width: self.view.frame.width, height: self.view.frame.height)
                self.tableView.frame = rect
                
                }, completion: {finished in
                    println(finished)
            })
            if sender != nil
            {
                self.view.bringSubviewToFront(sender)
            }
            
            listShown = true
        }
        else
        {
            UIView.animateWithDuration(0.2, delay: 0, options: .CurveEaseIn, animations: {
                
                var rect = CGRect(x: 0, y: 638, width: self.view.frame.width, height: self.view.frame.height)
                self.tableView.frame = rect
                
                }, completion: {finished in
                    println(finished)
            })
            
            if sender != nil
            {
                self.view.bringSubviewToFront(sender)
            }
            listShown = false
        }
        
    }
    
    
    
    func communityChange(sender:UIButton!)
    {
        currentCommunity = communities[sender.tag]
        self.mapView.removeAnnotations(mapView.annotations)
        loadCommunity([currentCommunity])
        toggleAddButton()
        self.menu.close()
        self.mapView.setCenterCoordinate(self.mapView.region.center, animated: false)
//        self.bottomButton.titleLabel?.text = currentCommunity
    }
    
    func setupLocationManager()
    {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!) {
//        println("locations = \(locations)")
        //1
        let location = CLLocationCoordinate2D(
            latitude: locationManager.location.coordinate.latitude,
            longitude: locationManager.location.coordinate.longitude
        )
        // 2
        let span = mapView.region.span
        let region = mapView.region
        
//        mapView.setRegion(region, animated: true)
        
        //3
//        let annotation = MKPointAnnotation()
//        annotation.setCoordinate(location)
//        annotation.title = "Big Ben"
//        annotation.subtitle = "London"
//        mapView.addAnnotation(annotation)
    }
    func toggleMenu()
    {
        if listShown
        {
            showListVIew(nil)
        }
        if self.menu.isOpen
        {
            self.menu.close()
            return
        }
//        self.menu.showFromNavigationController(self.navigationController)
        self.getCommunities()
        dispatch_async(dispatch_get_main_queue(), {
            self.menu.showInView(self.mapView)
        })
        

    }
    func toggleAddButton()
    {
        if currentCommunity == ""
        {
            self.navigationItem.rightBarButtonItem = nil
        }
        else
        {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "toggleAddMode")
            addMode = false
        }
    }
    func toggleAddMode()
    {
        
    
        if self.addMode
        {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Add, target: self, action: "toggleAddMode")
            addMode = false
            
        }
        else
        {
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.Done, target: self, action: "toggleAddMode")
            addMode = true
        }
        
        
    }
    func getUser()
    {
        let url = NSURL(string: "http://54.164.125.123/api/user/get")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["user_name": username]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
            data, response, error in
            if error != nil
            {
                println(error)
                return
            }
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: nil) as NSDictionary
            println(json)
            
        })
        uploadTask.resume()
    }
    func subscribe()
    {
        let url = NSURL(string: "http://54.164.125.123/api/user/subscribe")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["user_name": username,"community": currentCommunity]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
            data, response, error in
            if error != nil
            {
                println(error)
                return
            }
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: nil) as NSDictionary
            
        })
        uploadTask.resume()
    }
    
    func getCommunities()
    {
//        var list:NSMutableDictionary = NSMutableDictionary()
        var list: [String] = []
        let url = NSURL(string: "http://54.164.125.123/api/community/list")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["name":"devtesting1"]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler:
            {data, response, error in
//            println(data)
//            println(response)
//            println(error)
            
            if error != nil
            {
                println(error)
                self.getCommunities()
                return
            }
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: nil) as NSDictionary
            self.communities.removeAll(keepCapacity: true)
            self.annotations.removeAll(keepCapacity: true)
            for each in json
            {
                println(each)
                var data = each.value as NSDictionary
                
                self.communities.append(data["name"] as String)
//                print(list)
            }
            var views = self.scrollView.subviews
            for s in views
            {
                (s as UIView).removeFromSuperview()
            }
            
                
            dispatch_async(dispatch_get_main_queue(), {
            self.scrollView.contentSize = CGSize(width: Int(self.scrollView.frame.width), height:Int(70 * self.communities.count + 15))
            for var i = 0; i < self.communities.count; i++
            {
                var button: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
                button.frame = CGRect(x: 10, y: 70*i + 15, width: Int(self.scrollView.frame.width-20), height: 60)
                
                button.backgroundColor = UIColor.clearColor()
                
                button.addTarget(self, action: "communityChange:", forControlEvents: .TouchUpInside)
                button.setTitle(self.communities[i], forState: UIControlState.Normal)
                button.alpha = 1
                button.titleLabel?.text = self.communities[i]
                button.titleLabel?.textColor = UIColor.whiteColor()
                button.titleLabel?.font = UIFont(name: "Helvetica Neue", size: 25)
                button.tag = i
                self.scrollView.addSubview(button)
                self.scrollView.bringSubviewToFront(button)
            }
            })
                
            
            if self.currentCommunity == ""
            {
                self.loadMap()
                
            }
            self.getUser()
            
        })
        uploadTask.resume()
        
    }
    func addAnnotationonMap(lat: CLLocationDegrees, long: CLLocationDegrees, name:String, community: String, detail: String, id: Int, ep:Int)
    {
        let annotation = MKDetailAnnotation(coordinate: CLLocationCoordinate2D(latitude: lat, longitude: long), title: name, subtitle: community)
        annotation.setCoordinate(CLLocationCoordinate2D(latitude: lat, longitude: long))
        annotation.title = name
        annotation.subtitle = community
        annotation.detail = detail
        annotation.id = id
        annotation.ep = ep
        
        
        annotations.append(annotation)
        
        
        dispatch_async(dispatch_get_main_queue(), {
            self.mapView.addAnnotation(annotation)
        })
        
        
    }
    
    
    func addCommunity()
    {
        let url = NSURL(string: "http://54.164.125.123/api/community/add")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["name":"devtesting1"]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {data, response, error -> Void in
            
                        println(data)
                        println(response)
                        println(error)
            
            if error != nil
            {
                println(error)
                return
            }
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: nil) as NSDictionary
            
            
            
            
        })
        uploadTask.resume()
    }
    func addPoint(lat: CLLocationDegrees, long: CLLocationDegrees, name:String, detail:String)
    {
        if currentCommunity == ""
        {
            return
        }
        let loc = locationManager.location.coordinate
        
        let url = NSURL(string: "http://54.164.125.123/api/location/add")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["lat":lat,"long":long,"community":currentCommunity,"name":name,"description":detail]
//        var params: NSDictionary = ["name":"devtesting"]
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        let uploadTask = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
            data, response, error -> Void in
            
//            println(data)
//            println(response)
//            println(error)
            
            if error != nil
            {
                println(error)
                return
            }
            var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: nil) as NSDictionary
            self.loadCommunity([self.currentCommunity])
            
            
            
        })
        uploadTask.resume()
        
    }
    func loadCommunity(list: [String])
    {
        let loc = locationManager.location.coordinate
        let url = NSURL(string: "http://54.164.125.123/api/location/find")
        var urlRequest = NSMutableURLRequest(URL: url!)
        urlRequest.HTTPMethod = "POST"
        var params: NSDictionary = ["lat":loc.latitude,"long":loc.longitude,"communities": list]
        println(params["communities"])
        let postData = NSData(data: NSJSONSerialization.dataWithJSONObject(params, options: nil, error: nil)!)
        urlRequest.HTTPBody = postData
        urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        urlRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        let uploadTask = NSURLSession.sharedSession().uploadTaskWithRequest(urlRequest, fromData: postData) {data, response, error in
            var err: NSError?
            if error != nil
            {
                println(error)
                return
            }
            if err != nil
            {
                println(err)
                return
            }
                
            else
            {
                //                println(response)
                var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
                //                println(strData)
                
                var json = NSJSONSerialization.JSONObjectWithData(data, options: .MutableLeaves, error: &err) as? NSDictionary
                if let parseJson = json
                {
                    //                    let data = parseJson["1"] as NSDictionary
                    
                    self.mapView.removeAnnotations(self.mapView.annotations)
                    self.annotations.removeAll(keepCapacity: true)
                    
                    for dict in parseJson
                    {
                        //
                        
                        let data = dict.value as NSDictionary
                        var lat:CLLocationDegrees = CLLocationDegrees(data["lat"] as Float)
                        var long: CLLocationDegrees = CLLocationDegrees(data["long"] as Float)
                        var name: String = data["name"] as String
                        var community: String = data["community"] as String
                        var detail: String = data["description"] as String
                        var id: Int = data["id"] as Int
                        var eneoPts: Int = data["ep"] as Int
                        
                        self.addAnnotationonMap(lat, long: long, name: name, community: community, detail:detail, id: id, ep:eneoPts)
                        
                        
                        
                    }

                    
                }
                //                println(json)
            }
            
        }
        uploadTask.resume()
//        self.mapView.setCenterCoordinate(self.mapView.region.center, animated: false)
    }
    func loadMap()
    {
        loadCommunity(communities)
        currentCommunity = ""
        toggleAddButton()
//        self.mapView.setCenterCoordinate(self.mapView.region.center, animated: false)
       
    }
    func addPin(gestureRecognizer: UIGestureRecognizer)
    {
        if currentCommunity != "" && gestureRecognizer.state == UIGestureRecognizerState.Began && addMode
        {
            var point = gestureRecognizer.locationInView(self.mapView)
            var loc: CLLocationCoordinate2D = self.mapView.convertPoint(point, toCoordinateFromView: self.view)
            println(loc)
            toggleAddMode()
            tempLocation = loc
            
            makeAddView()
//            addPoint(CLLocationDegrees(loc.latitude), long: CLLocationDegrees(loc.longitude))
//            dispatch_async(dispatch_get_main_queue(), {
//                self.loadCommunity([self.currentCommunity])
//            })
            
            println("shit happens")
            self.menu.directionUp = false
//            self.menu.showInView(mapView)
            self.menu.directionUp = true
            
        }
        
        
    }
    func makeAddView()
    {
        var addView = UIView()
        addView.layer.cornerRadius = 6
        addView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.width-80, height: 400))
        addView.backgroundColor = UIColor.clearColor()
        addView.alpha = 1
        var nameLabel: UILabel = UILabel(frame: CGRect(x: (addView.frame.width-70)/2, y: 20, width: addView.frame.width - 50, height: 30))
        nameLabel.text = "Add a point!"

        var textField: UITextField = UITextField(frame: CGRect(x: 30, y: 60, width: Int(addView.frame.width - 50), height: 30))
        textField.backgroundColor = UIColor.whiteColor()
        textField.borderStyle = UITextBorderStyle.RoundedRect
        textField.placeholder = "Enter a name"
        tempTextField = textField
        var textBox: UITextView = UITextView(frame: CGRect(x: 30, y: 110, width: Int(addView.frame.width - 50), height: 150))
        textBox.layer.cornerRadius = 8.0
        textBox.layer.borderWidth = 0.1
        textBox.layer.masksToBounds = true
        
        tempTextBox = textBox
        
        var button: UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        button.frame = CGRect(x: (addView.frame.width-50)/2, y: 290, width: 50, height: 50)
        button.setImage(UIImage(named: "logo100"), forState: .Normal)
        button.backgroundColor = UIColor.whiteColor()
        
        button.addTarget(self, action: "addLocation" , forControlEvents: .TouchUpInside)
        
        addView.addSubview(nameLabel)
        addView.addSubview(textField)
        addView.addSubview(textBox)
        addView.addSubview(button)
        
        
        
        var menuitem2: REMenuItem = REMenuItem(customView: addView)
        self.addMenu = REMenu(items: [menuitem2])
        self.addMenu.itemHeight = 400
        self.addMenu.bounce = false
        self.addMenu.backgroundColor = UIColor.whiteColor()
        self.addMenu.backgroundAlpha = 1.0
        self.addMenu.appearsBehindNavigationBar = false
        self.addMenu.liveBlur = true
//        self.addMenu.containerView.alpha = 1.0
        self.addMenu.liveBlurTintColor = UIColor.grayColor()
        self.addMenu.borderColor = UIColor.clearColor()
        
        self.addMenu.showInView(self.mapView)
        self.addMenu.containerView.alpha = 1.0
    }
    func addLocation()
    {
        
        addPoint(tempLocation.latitude, long: tempLocation.longitude, name:tempTextField.text,detail:tempTextBox.text)
        dispatch_async(dispatch_get_main_queue(), {
            
        })
        
        self.addMenu.close()
    }
    func mapView(mapView: MKMapView!, didSelectAnnotationView view: MKAnnotationView!) {
        
        mapView.deselectAnnotation(view.annotation, animated: true)
        
        
        var viewController = storyboard?.instantiateViewControllerWithIdentifier("DetailViewController") as DetailViewController
        let annotation = view.annotation as MKDetailAnnotation
        
        viewController.annotation = annotation
//        println(annotation.title)
        self.navigationController?.pushViewController(viewController, animated: true)
        
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if annotation.isKindOfClass(MKUserLocation)
        {
            return nil
        }
        else if annotation.isKindOfClass(MKPointAnnotation)
        {
            let identifier: String = "CustomAnnotation"
            var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier(identifier)
            if (annotationView != nil)
            {
                annotationView.annotation = annotation
            }
            else
            {
                annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                
            }
            annotationView.canShowCallout = false
            return annotationView
        }
        return nil
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return annotations.count
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
        
        
        var title: String = annotations[indexPath.row].title
        cell.textLabel?.text = title
        cell.backgroundColor = UIColor.grayColor()
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        var viewController = storyboard?.instantiateViewControllerWithIdentifier("DetailViewController") as DetailViewController
        let annotation = annotations[indexPath.row]
        
        let region = MKCoordinateRegion(center: annotation.coordinate, span: mapView.region.span)
        
        mapView.setRegion(region, animated: true)
        showListVIew(nil)
//        viewController.annotation = annotation
//        //        println(annotation.title)
//        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    
    
    
    


}

